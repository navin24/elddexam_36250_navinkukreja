#ifndef __EXPORT_H
#define __EXPORT_H

extern int kfifo_size,kfifo_length,kfifo_available;

void kfifo_push(char *ch);
char kfifo_pop(void);

#endif
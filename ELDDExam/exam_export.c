#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kfifo.h>

int size = 30;
struct kfifo mybuf;
int kfifo_size,kfifo_length,kfifo_available;

void kfifo_push(char *ch)
{
    int ret;
    char buffer[5];
    ret = kfifo_in(&mybuf,buffer,5);
    printk("\nCharacter = %s\t.\n",ch);
    kfifo_available = kfifo_length - sizeof(ch);    
    kfifo_length = kfifo_size - kfifo_available;
}
 char kfifo_pop(void)
 {
     int ret;
     char buffer[5];
     char cp[5] = {'N','V','N'};
      printk("Pop.\n");
      ret = kfifo_out(&mybuf,buffer,5);
      printk("\nCharacter returned = %s\t.\n",cp);
      kfifo_available = kfifo_length - sizeof(cp);
    kfifo_length = kfifo_size - kfifo_available;
      return buffer;
 }

static int __init export_init(void)
{
    int ret;
    printk(KERN_INFO "%s: Exam Export Started.\n",THIS_MODULE->name);
    //dynamic allocation of kfifo
    ret = kfifo_alloc(&mybuf , size , GFP_KERNEL);
    if(ret!=0)
    {
        printk(KERN_INFO "%s: kfifo() failed.\n",THIS_MODULE->name);
        return -1;
    }
    if(size>1024)
    printk(KERN_INFO "%s: kfifo() allocated memory is out of bounds.\n",THIS_MODULE->name);
    else
    {
        printk(KERN_INFO "%s: kfifo() allocated memory is %d .\n",THIS_MODULE->name,size);
    }
    kfifo_size = kfifo_size(&mybuf);
    kfifo_length = kfifo_size(&mybuf);
    kfifo_available = kfifo_size(&mybuf);

    return 0;
}

static void __exit export_exit(void)
{
    printk(KERN_INFO "%s: Exam Export Ended.\n",THIS_MODULE->name);
    printk(KERN_INFO "%s: KFIFO_SIZE    :   %d.\n",THIS_MODULE->name,kfifo_size);
    printk(KERN_INFO "%s: KFIFO_LENGTH     %d.\n",THIS_MODULE->name,kfifo_length);
    printk(KERN_INFO "%s: KFIFO_AVAILABLE   :   %d.\n",THIS_MODULE->name,kfifo_available);
    kfifo_free(&mybuf);
    printk(KERN_INFO "%s: KFIFO_SIZE    :   %d.\n",THIS_MODULE->name,kfifo_size);
    printk(KERN_INFO "%s: KFIFO_LENGTH     %d.\n",THIS_MODULE->name,kfifo_length);
    printk(KERN_INFO "%s: KFIFO_AVAILABLE   :   %d.\n",THIS_MODULE->name,kfifo_available);
}

module_init(export_init);
module_exit(export_exit);

module_param(size,int,0600);

EXPORT_SYMBOL(kfifo_size);
EXPORT_SYMBOL(kfifo_length);
EXPORT_SYMBOL(kfifo_available);
EXPORT_SYMBOL(kfifo_push);
EXPORT_SYMBOL(kfifo_pop);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Navin@36250 <navin.kukreja@gamil.com");
MODULE_DESCRIPTION("Module Parameter and kfifo");
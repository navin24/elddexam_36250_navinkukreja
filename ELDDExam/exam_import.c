#include <linux/module.h>
#include "exam_export.h"

static int __init import_init(void)
{
    char cp[5] = {'N','A','V','I','N'};
    printk(KERN_INFO "%s: Exam Import Started.\n",THIS_MODULE->name);
    printk(KERN_INFO "%s: KFIFO_SIZE    :   %d.\n",THIS_MODULE->name,kfifo_size);
    printk(KERN_INFO "%s: KFIFO_LENGTH     %d.\n",THIS_MODULE->name,kfifo_length);
    printk(KERN_INFO "%s: KFIFO_AVAILABLE   :   %d.\n",THIS_MODULE->name,kfifo_available);
    kfifo_push(cp);
    printk(KERN_INFO "%s: KFIFO_SIZE    :   %d.\n",THIS_MODULE->name,kfifo_size);
    printk(KERN_INFO "%s: KFIFO_LENGTH     %d.\n",THIS_MODULE->name,kfifo_length);
    printk(KERN_INFO "%s: KFIFO_AVAILABLE   :   %d.\n",THIS_MODULE->name,kfifo_available);
    return 0;
}

static void __exit import_exit(void)
{
    printk(KERN_INFO "%s: Exam Import Ended.\n",THIS_MODULE->name);
    printk(KERN_INFO "%s: KFIFO_SIZE    :   %d.\n",THIS_MODULE->name,kfifo_size);
    printk(KERN_INFO "%s: KFIFO_LENGTH     %d.\n",THIS_MODULE->name,kfifo_length);
    printk(KERN_INFO "%s: KFIFO_AVAILABLE   :   %d.\n",THIS_MODULE->name,kfifo_available);
    kfifo_pop();
    printk(KERN_INFO "%s: KFIFO_SIZE    :   %d.\n",THIS_MODULE->name,kfifo_size);
    printk(KERN_INFO "%s: KFIFO_LENGTH     %d.\n",THIS_MODULE->name,kfifo_length);
    printk(KERN_INFO "%s: KFIFO_AVAILABLE   :   %d.\n",THIS_MODULE->name,kfifo_available);
}

module_init(import_init);
module_exit(import_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Navin@36250 <navin.kukreja@gamil.com");
MODULE_DESCRIPTION("Module Parameter and kfifo");